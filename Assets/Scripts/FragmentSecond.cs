﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentSecond : Bullet {

    public int maxAmmo = 3;
    Cartridge cartridge;
    public int timeToExplode = 2;
    private float currentTime = 0f;

    void Awake()
    {
        cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[0];
    }

    protected new virtual void Update()
    {
        base.Update();
        if (shooting)
        {
            currentTime += Time.deltaTime;
            if (currentTime > timeToExplode)
            {
                ShotMiniBullets();
            }
        }
        else
        {
            currentTime = 0f;
        }
    }

    void ShotMiniBullets()
    {
        Debug.Log("Shot mini bullets");
        float z = transform.rotation.eulerAngles.z;
        cartridge.GetBullet().Shot(transform.position, -50 + z);
        cartridge.GetBullet().Shot(transform.position, 0 + z);
        cartridge.GetBullet().Shot(transform.position, -50 + z);

        Reset();
    }
}